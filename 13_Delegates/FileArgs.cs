﻿using System;

namespace _13_Delegates
{
    public class FileArgs: EventArgs
    {
        public string FileName { get; set; }
    }
}