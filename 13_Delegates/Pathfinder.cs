﻿using System;
using System.IO;

namespace _13_Delegates
{
    public class Pathfinder
    {
        public event EventHandler<FileArgs> FileFound;
        public void Do()
        {
            string dirName = "C:\\";
            if (Directory.Exists(dirName))
            {                
                string[] files = Directory.GetFiles(dirName);
                foreach (string s in files)
                {
                    FileFound?.Invoke(this, new FileArgs() { FileName = s });                    
                }
            }
        }
    }
}
