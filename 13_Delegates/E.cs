﻿using System;
using System.Collections.Generic;

namespace _13_Delegates
{
    public static class E
    {
        // обобщённая функция расширения, находящую 
        //и возвращающую максимальный элемент коллекции
        public static T FindMax<T>(this List<T> list, Func<T, int> func)
        {
            var result = list[0];
            var i = int.MinValue;
            foreach (var item in list)
            {
                var v = func(item);
                if (v > i)
                {
                    i = v;
                    result = item;
                }
            }
            return result;
        }
    }    
}
