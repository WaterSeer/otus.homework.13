﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _13_Delegates
{
    public class Program
    {       
        static void Main(string[] args)
        {
            //Форморование событий при нахождении файла
            var pathfinder = new Pathfinder();
            pathfinder.FileFound += Pathfinder_FileFound;
            pathfinder.Do();
            pathfinder.FileFound -= Pathfinder_FileFound;            
            Console.ReadLine();

            //поиск максимального (формат поиска задаётся функцией) элемента коллекции
            List<char> myList = new List<char>() { 'a', 'g', 'm', 'k', 'y', 'r', 'g' };
            Console.WriteLine("Дана коллекция типа int");
            //вывод на экран элементы коллекции
            StringBuilder sb = new StringBuilder();
            foreach (var item in myList)
            {
                sb.Append(item + " ");
            }
            Console.WriteLine(sb.ToString());

            var m = myList.FindMax(x => (int)x);
            Console.WriteLine("Максимальный элемент коллекции: "+m.ToString());

            Console.ReadLine();
        }

        //в аргументе "FileArgs e" передаю название файла для вывода на консоль 
        private static void Pathfinder_FileFound(object sender, FileArgs e)
        {
            Console.WriteLine("Файл: " + e.FileName.ToString());
        }
    }
}
